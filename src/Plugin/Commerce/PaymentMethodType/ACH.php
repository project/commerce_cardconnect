<?php

namespace Drupal\commerce_cardconnect\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Annotation\CommercePaymentMethodType;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the ACH payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_cardconnect_ach",
 *   label = @Translation("CardConnect ACH"),
 *   create_label = @Translation("New ACH Mandate"),
 * )
 *
 * @package Drupal\commerce_cardconnect\Plugin\Commerce\PaymentMethodType
 */
class ACH extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method)  {
    return $this->t(
      'Bank Account ending @account',
      [
        '@account' => $payment_method->account_no->value,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['account_no'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Account Number'))
      ->setDescription($this->t('The last few digits of the account number'))
      ->setRequired(TRUE);

    return $fields;
  }
}
