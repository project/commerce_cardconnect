<?php

namespace Drupal\commerce_cardconnect\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;

/**
 * Payment Gateway for integrating with cardconnect
 *
 * @CommercePaymentGateway(
 *   id = "cardconnect",
 *   label = "CardConnect",
 *   display_label = "CardConnect",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_cardconnect\PluginForm\GoCardlessPaymentMethodAddForm",
 *   },
 *   modes = {
 *     "uat" = "Testing",
 *     "prod" = "Production",
 *   },
 *   payment_method_types = {"credit_card", '"commerce_cardconnect_ach"},
 * )
 *
 * @package Drupal\commerce_cardconnect\Plugin\Commerce\PaymentGateway
 */
class CardConnect extends OnsitePaymentGatewayBase {



  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

    // TODO: Implement createPaymentMethod() method.
  }

  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // TODO: Implement deletePaymentMethod() method.
  }

  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // TODO: Implement createPayment() method.
  }

}
